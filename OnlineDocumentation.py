import sublime, sublime_plugin

def open_url(url):
	sublime.active_window().run_command('open_url', {"url": url})


class OnlineDocumentationCommand(sublime_plugin.TextCommand):

	def run(self, edit):
		selection = self.view.sel()
		word = self.view.word(selection[0])
		word_string = self.view.substr(word)
		scope = self.view.scope_name(word.begin()).strip()
		extracted_scope = scope.rpartition('.')[2]
		getattr(self, '%s_doc' % extracted_scope, self.unsupported)(word_string, scope)

	def unsupported(self, keyword, scope):
			sublime.status_message("This scope is not supported: %s" % scope.rpartition('.')[2])

	def c_doc(self, keyword, scope):
		open_url("http://google.com/search?q=C+%s" % keyword)

	def php_doc(self, keyword, scope):
		open_url("http://php.net/%s" % keyword)

	def rails_doc(self, keyword, scope):
		open_url("http://api.rubyonrails.org/?q=%s" % keyword)

	def controller_doc(self, keyword, scope):
		open_url("http://api.rubyonrails.org/?q=%s" % keyword)

	def ruby_doc(self, keyword, scope):
		open_url("http://api.rubyonrails.org/?q=%s" % keyword)

	def js_doc(self, keyword, scope):
		open_url("https://developer.mozilla.org/en-US/search?q=%s" % keyword)

	coffee_doc = js_doc

	def python_doc(self, keyword, scope):
		open_url("http://docs.python.org/search.html?q=%s" % keyword)

	def clojure_doc(self, keyword, scope):
		open_url("http://clojuredocs.org/search?x=0&y=0&q=%s" % keyword)

	def go_doc(self, keyword, scope):
		open_url("http://golang.org/search?q=%s" % keyword)

	def swift_doc(self, keyword, scope):
		open_url("https://developer.apple.com/search/?q=%s" % keyword)

	def smarty_doc(self, keyword, scope):
		open_url('http://www.smarty.net/%s' % keyword)

	def cmake_doc(self, keyword, scope):
		open_url('http://cmake.org/cmake/help/v2.8.8/cmake.html#command:%s' % keyword.lower())

	def perl_doc(self, keyword, scope):
		open_url("http://perldoc.perl.org/search.html?q=%s" % keyword)
