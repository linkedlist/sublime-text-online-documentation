Sublime Text Online Documentation
=================================
Code taken from [kemayo/sublime-text-2-goto-documentation](https://github.com/kemayo/sublime-text-2-goto-documentation "sublime-text-2-goto-documentation")  
Works in Sublime Text 3  

You must create your own key binding e.g.  
`{ "keys": ["f1"], "command": "online_documentation" }`